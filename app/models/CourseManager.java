package models;

import models.entities.Course;
import play.db.DB;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by SezerToprak on 15.06.2016.
 */
public class CourseManager {

    public static boolean addCourse(String courseName){
        Connection c = DB.getConnection();
        PreparedStatement statement = null;

        try{
            statement = c.prepareStatement("INSERT  INTO course(coursename) VALUES (?)");
            statement.setString(1,courseName);
            int result = statement.executeUpdate();
            if(result>0)
                return true;

        }catch (SQLException e){
            e.printStackTrace();
        }
        return false;
    }

    public static boolean addCourse(Course course){
        boolean result = addCourse(course.getCourseName());
        return  result;
    }

    public static Course getCourse(int courseId){
        Course result = null;

        try(Connection c=DB.getConnection()){
            try(PreparedStatement s=c.prepareStatement("SELECT * FROM course WHERE courseid =?")){
                s.setInt(1,courseId);

                try(ResultSet resultSet=s.executeQuery()){
                    if (resultSet.next()){
                        result=new Course();
                        result.setCourseName(resultSet.getString("coursename"));
                        result.setCourseId(resultSet.getInt("courseid"));
                    }

                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    public static List<Course> getCourses(){
        List<Course> result  = new ArrayList<Course>();

        try (Connection connection= DB.getConnection()){
            try(Statement statement=connection.createStatement()){
                try (ResultSet resultSet=statement.executeQuery("SELECT * FROM course")){
                    while(resultSet.next()){
                        Course course=new Course();
                        course.setCourseName(resultSet.getString("coursename"));
                        course.setCourseId(resultSet.getInt("courseid"));
                        result.add(course);
                    }
                }

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static boolean deleteCourse(int courseId){
        try (Connection connection= DB.getConnection()){
            try(PreparedStatement statement=connection.prepareStatement("DELETE FROM course WHERE courseid =?")){
                statement.setInt(1,courseId);
                int result=statement.executeUpdate();
                if(result>0)
                    return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return  false;
    }
}
