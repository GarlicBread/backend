package models;

import models.relations.Uni_Dep;
import play.db.DB;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by SezerToprak on 16.06.2016.
 */
public class Uni_DepManager {

    public static boolean addUniDep(int universityId, int departmentId){
        Connection c = DB.getConnection();
        PreparedStatement statement = null;

        try{
            statement = c.prepareStatement("INSERT  INTO uni_dep(universityid,departmentid) VALUES (?,?)");
            statement.setInt(1,universityId);
            statement.setInt(2,departmentId);
            int result = statement.executeUpdate();
            if(result>0)
                return true;

        }catch (SQLException e){
            e.printStackTrace();
        }
        return false;
    }

    public static boolean addUniDep(Uni_Dep uniDep){
        boolean result = addUniDep(uniDep.getUniversityId(),uniDep.getDepartmentId());
        return result;
    }

    public static Uni_Dep getUniDep(int uniDepId){
        Uni_Dep result = null;

        try(Connection c=DB.getConnection()){
            try(PreparedStatement s=c.prepareStatement("SELECT * FROM uni_dep WHERE unidepid =?")){
                s.setInt(1,uniDepId);

                try(ResultSet resultSet=s.executeQuery()){
                    if (resultSet.next()){
                        result=new Uni_Dep();
                        result.setUniDepId(resultSet.getInt("unidepid"));
                        result.setUniversityId(resultSet.getInt("univeristyid"));
                        result.setDepartmentId(resultSet.getInt("departmentid"));
                    }

                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    public static List<Uni_Dep> getUniDeps(){
        List<Uni_Dep> result = new ArrayList<Uni_Dep>();

        try (Connection connection= DB.getConnection()){
            try(Statement statement=connection.createStatement()){
                try (ResultSet resultSet=statement.executeQuery("SELECT * FROM uni_dep")){
                    while(resultSet.next()){
                        Uni_Dep unidep=new Uni_Dep();
                        unidep.setUniDepId(resultSet.getInt("unidepid"));
                        unidep.setUniversityId(resultSet.getInt("universityid"));
                        unidep.setDepartmentId(resultSet.getInt("departmentid"));
                        result.add(unidep);
                    }
                }

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return  result;
    }

    public static boolean deleteUniDep(int uniDepId){
        try (Connection connection= DB.getConnection()){
            try(PreparedStatement statement=connection.prepareStatement("DELETE FROM uni_dep WHERE unidepid =?")){
                statement.setInt(1,uniDepId);
                int result=statement.executeUpdate();
                if(result>0)
                    return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return  false;
    }
}
