package models;

import models.entities.Note;
import play.db.DB;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static play.mvc.Controller.request;

//import java.sql.*;

/**
 * Created by SezerToprak on 19.06.2016.
 */
public class NoteManager {

    public static boolean addNote(String userName,int courseUniDepId,String title,String text,boolean state,java.util.Date date)
    {
        Connection c = DB.getConnection();
        PreparedStatement statement = null;

        try{
            statement = c.prepareStatement("INSERT  INTO notes(username,courseunidepid,title,text,state,date) "+
                    " VALUES(?,?,?,?,?,?)");
            statement.setString(1,userName);
            statement.setInt(2,courseUniDepId);
            statement.setString(3,title);
            statement.setString(4,text);
            statement.setBoolean(5,state);
            statement.setDate(6, new java.sql.Date(date.getTime()));

            int result = statement.executeUpdate();
            if(result>0)
                return true;

        }catch (SQLException e){
            e.printStackTrace();
        }
        return false;
    }
    public static boolean addNote(Note note){

        if(addNote(note.getUserName(), note.getCourseUniDepId(), note.getTitle(), note.getText(), note.getState(), note.getDate())){
            return true;
        }

        return false;
    }

    public static Note getNote(int noteId){
        Note result = null;

        try(Connection c=DB.getConnection()){
            try(PreparedStatement s=c.prepareStatement("SELECT * FROM noteswithvotes WHERE noteid =?")){
                s.setInt(1,noteId);

                try(ResultSet resultSet=s.executeQuery()){
                    if (resultSet.next()){
                        result=new Note(resultSet);

                    }

                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    public static List<Note> getNotes(){
        List<Note> result  = new ArrayList<Note>();

        try (Connection connection= DB.getConnection()){
            try(Statement statement=connection.createStatement()){
                try (ResultSet resultSet=statement.executeQuery("SELECT * FROM noteswithvotes")){
                    while(resultSet.next()){
                        Note note=new Note(resultSet);
                        result.add(note);
                    }
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static boolean deleteNote(int noteId){
        try (Connection connection= DB.getConnection()){
            try(PreparedStatement statement=connection.prepareStatement("DELETE FROM notes WHERE noteid =?")){
                statement.setInt(1,noteId);
                int result=statement.executeUpdate();
                if(result>0)
                    return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return  false;
    }

    public static boolean addMediaToNote(Note note, String mediaPath)throws IOException{
        boolean result=false;
        int mediaType=1;
        String mediaTitle="Title";

        File pic = (File) request().body().asMultipartFormData().getFiles().get(0).getFile();
        String fileName = org.apache.commons.lang3.RandomStringUtils.random(10, "abcdefdgaawqewqe12345908841");
        File output = new File(mediaPath + fileName + ".pdf");
        FileOutputStream ou = null;
        try {
            ou = new FileOutputStream(output);
            FileInputStream io = new FileInputStream(pic);
            ou.getChannel().transferFrom(io.getChannel(), 0, io.getChannel().size());
            MediaManager.addMedia(note.getNodeId(),mediaType,mediaTitle,mediaPath + fileName + ".pdf");
            result= true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally{
            ou.getChannel().close();
        }
        return result;
    }
}
