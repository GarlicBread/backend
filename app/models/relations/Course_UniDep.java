package models.relations;

/**
 * Created by SezerToprak on 16.06.2016.
 */
public class Course_UniDep {
    private int courseUniDepId;
    private int uniDepId;
    private int courseId;

    public int getCourseUniDepId(){return courseUniDepId;}
    public void setCourseUniDepId(int courseUniDepId){this.courseUniDepId = courseUniDepId;}

    public int getUniDepId(){return uniDepId;}
    public void setUniDepId(int uniDepId){this.uniDepId=uniDepId;}

    public int getCourseId(){return courseId;}
    public void setCourseId(int courseId){this.courseId = courseId;}
}
