package models.relations;

/**
 * Created by SezerToprak on 16.06.2016.
 */
public class Uni_Dep {
    private int uniDepId;
    private int universityId;
    private int departmentId;

    public int getUniDepId(){return uniDepId;}
    public void setUniDepId(int uniDepId){this.uniDepId = uniDepId;}

    public int getUniversityId(){ return universityId;}
    public void setUniversityId(int universityId){ this.universityId = universityId;}

    public int getDepartmentId(){return departmentId;}
    public void setDepartmentId(int departmentId){this.departmentId = departmentId;}
}
