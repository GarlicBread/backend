package models;

import models.entities.Media;
import play.db.DB;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by SezerToprak on 20.06.2016.
 */
public class MediaManager {
    public static boolean addMedia(int noteId,int mediaTypeId,String mediaTitle,String mediaPath){
        Connection c = DB.getConnection();
        PreparedStatement statement = null;

        try{
            statement = c.prepareStatement("INSERT  INTO media(noteid,mediatypeid,mediatitle,mediapath) "+
                    " VALUES (?,?,?,?)");
            statement.setInt(1,noteId);
            statement.setInt(2,mediaTypeId);
            statement.setString(3,mediaTitle);
            statement.setString(4,mediaPath);
            int result = statement.executeUpdate();
            if(result>0)
                return true;

        }catch (SQLException e){
            e.printStackTrace();
        }
        return false;
    }

    public static boolean addMedia(Media media){
        return addMedia(media.getNoteId(),media.getMediaTypeId(),media.getMediaTitle(),media.getMediaPath());
    }

    public static Media getMediaByMediaId(int mediaId){
        Media result = null;

        try(Connection c=DB.getConnection()){
            try(PreparedStatement s=c.prepareStatement("SELECT * FROM media WHERE mediaid =?")){
                s.setInt(1,mediaId);

                try(ResultSet resultSet=s.executeQuery()){
                    if (resultSet.next()){
                        result=new Media(resultSet);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }
    /// get all attached media of a post
    public static List<Media> getMediaByNoteId(int noteId){
        List<Media> result  = new ArrayList<Media>();

        try (Connection connection= DB.getConnection()){
            try(PreparedStatement statement=connection.prepareStatement("SELECT * FROM media WHERE noteid=?")){
                statement.setInt(1,noteId);
                try (ResultSet resultSet=statement.executeQuery()){
                    while(resultSet.next()){
                        Media media=new Media(resultSet);
                        result.add(media);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static List<Media> getAllMedia(){
        List<Media> result  = new ArrayList<Media>();

        try (Connection connection= DB.getConnection()){
            try(Statement statement=connection.createStatement()){
                try (ResultSet resultSet=statement.executeQuery("SELECT * FROM media")){
                    while(resultSet.next()){
                        Media media=new Media(resultSet);
                        result.add(media);
                    }
                }

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static boolean deleteMedia(int mediaId){
        try (Connection connection= DB.getConnection()){
            try(PreparedStatement statement=connection.prepareStatement("DELETE FROM media WHERE mediaid =?")){
                statement.setInt(1,mediaId);
                int result=statement.executeUpdate();
                if(result>0)
                    return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return  false;
    }
}
