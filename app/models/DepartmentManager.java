package models;

import models.entities.Department;
import play.db.DB;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by SezerToprak on 15.06.2016.
 */
public class DepartmentManager {

    public static boolean addDepartment(String departmentName){
        Connection c = DB.getConnection();
        PreparedStatement statement = null;

        try{
            statement = c.prepareStatement("INSERT  INTO department(departmentname) VALUES (?)");
            statement.setString(1,departmentName);
            int result = statement.executeUpdate();
            if(result>0)
                return true;

        }catch (SQLException e){
            e.printStackTrace();
        }
        return false;
    }

    public static boolean addDepartment(Department department){
        boolean result = addDepartment(department.getDepartmentName());
        return  result;
    }

    public static Department getDepartment(int id){
        Department result = null;

        try(Connection c=DB.getConnection()){
            try(PreparedStatement s=c.prepareStatement("SELECT * FROM department WHERE departmentid =?")){
                s.setInt(1,id);

                try(ResultSet resultSet=s.executeQuery()){
                    if (resultSet.next()){
                        result=new Department();
                        result.setDepartmentName(resultSet.getString("departmentname"));
                        result.setDepartmentId(resultSet.getInt("departmentid"));
                    }

                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

            return result;
    }

    public static List<Department> getDepartments(){
        List<Department> result  = new ArrayList<Department>();

        try (Connection connection= DB.getConnection()){
            try(Statement statement=connection.createStatement()){
                try (ResultSet resultSet=statement.executeQuery("SELECT * FROM department")){
                    while(resultSet.next()){
                        Department dep=new Department();
                        dep.setDepartmentName(resultSet.getString("departmentname"));
                        dep.setDepartmentId(resultSet.getInt("departmentid"));
                        result.add(dep);
                    }
                }

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static boolean deleteDepartment(int departmentId){
        try (Connection connection= DB.getConnection()){
            try(PreparedStatement statement=connection.prepareStatement("DELETE FROM department WHERE departmentid =?")){
                statement.setInt(1,departmentId);
                int result=statement.executeUpdate();
                if(result>0)
                    return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return  false;
    }
}
