package models;

import models.actors.Admin;
import models.helpers.Password;
import play.db.DB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * Created by kozo on 6/20/16.
 */
public class AdminManager {
    public static boolean add(String userName, String password
                , String name, String surname, String email
                , Date birthday, String birthplace, String bio, boolean status, Date date) {

        String sql="INSERT INTO admin(username, name, surname, email, birthday,birthplace, password, bio, date, status) VALUES " +
                "(?,?,?,?,?,?,?,?,?,?)";
        try(Connection con= DB.getConnection()){
            try(PreparedStatement statement=con.prepareStatement(sql)){
                int i=1;
                statement.setString(i++,userName);
                statement.setString(i++,name);
                statement.setString(i++,surname);
                statement.setString(i++,email);
                statement.setDate(i++,new java.sql.Date(birthday.getTime()));
                statement.setString(i++,birthplace);
                statement.setString(i++, Password.getSaltedHash(password));
                statement.setString(i++,bio);
                statement.setDate(i++,new java.sql.Date(date.getTime()));
                statement.setBoolean(i++,status);
                return statement.executeUpdate()>0;
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return  false;
    }

    public static Admin getAdminByUserNameAndPassword(String userName, String password) {
        Admin result=null;

        try (Connection con= DB.getConnection()){

            try (PreparedStatement statement=con.prepareStatement("SELECT  * FROM admin WHERE username=?")){
                statement.setString(1,userName);
                try(ResultSet set=statement.executeQuery()) {
                    if(set.next()){
                        String hasshedPassword=set.getString("password");
                        if(Password.check(password,hasshedPassword)){
                            return  new Admin(set);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return  result;
    }
}
