package models;

import models.relations.Course_UniDep;
import play.db.DB;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by SezerToprak on 16.06.2016.
 */
public class Course_UniDepManager {

    public static boolean addCourseUniDep(int uniDepId, int courseId){
        Connection c = DB.getConnection();
        PreparedStatement statement = null;

        try{
            statement = c.prepareStatement("INSERT  INTO course_unidep(unidepid,courseid) VALUES (?,?)");
            statement.setInt(1,uniDepId);
            statement.setInt(2,courseId);
            int result = statement.executeUpdate();
            if(result>0)
                return true;

        }catch (SQLException e){
            e.printStackTrace();
        }
        return false;
    }

    public static boolean addCourseUniDep(Course_UniDep courseUniDep){
        boolean result = addCourseUniDep(courseUniDep.getUniDepId(),courseUniDep.getCourseId());
        return result;
    }

    public static Course_UniDep getCourseUniDep(int courseUniDepId){
        Course_UniDep result = null;

        try(Connection c=DB.getConnection()){
            try(PreparedStatement s=c.prepareStatement("SELECT * FROM course_unidep WHERE courseunidepid =?")){
                s.setInt(1,courseUniDepId);

                try(ResultSet resultSet=s.executeQuery()){
                    if (resultSet.next()){
                        result=new Course_UniDep();
                        result.setCourseUniDepId(resultSet.getInt("courseunidepid"));
                        result.setUniDepId(resultSet.getInt("unidepid"));
                        result.setCourseId(resultSet.getInt("courseid"));
                    }

                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    public static List<Course_UniDep> getCourseUniDeps(){
        List<Course_UniDep> result = new ArrayList<Course_UniDep>();

        try (Connection connection= DB.getConnection()){
            try(Statement statement=connection.createStatement()){
                try (ResultSet resultSet=statement.executeQuery("SELECT * FROM course_unidep")){
                    while(resultSet.next()){
                        Course_UniDep courseUniDep=new Course_UniDep();
                        courseUniDep.setCourseUniDepId(resultSet.getInt("courseunidepid"));
                        courseUniDep.setUniDepId(resultSet.getInt("unidepid"));
                        courseUniDep.setCourseId(resultSet.getInt("courseid"));
                        result.add(courseUniDep);
                    }
                }

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return  result;
    }

    public static boolean deleteCourseUniDep(int courseUniDepId){
        try (Connection connection= DB.getConnection()){
            try(PreparedStatement statement=connection.prepareStatement("DELETE FROM course_unidep WHERE courseunidep =?")){
                statement.setInt(1,courseUniDepId);
                int result=statement.executeUpdate();
                if(result>0)
                    return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return  false;
    }
}
