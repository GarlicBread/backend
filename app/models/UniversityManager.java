package models;

import models.entities.University;
import play.db.DB;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kozo on 6/10/16.
 */
public class UniversityManager {

    public static boolean addUniversity(String universityName) {
        Connection c= DB.getConnection();
        PreparedStatement statement= null;
        try {
            statement = c.prepareStatement("INSERT  INTO university(universityname) VALUES (?)");
            statement.setString(1,universityName);
            int result=statement.executeUpdate();
            if(result>0)
                return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return  false;
    }

    public static boolean addUniversity(University university){
        boolean result = addUniversity(university.getUniversityName());
        return result;
    }

    public  static University getUniversity(int id){
        University result=null;
        try(Connection c=DB.getConnection()){
            try(PreparedStatement s=c.prepareStatement("SELECT * FROM university WHERE universityid =?")){
               s.setInt(1,id);

                try(ResultSet resultSet=s.executeQuery()){
                    if (resultSet.next()){
                        result=new University();
                        result.setUniversityName(resultSet.getString("universityname"));
                        result.setUniversityId(resultSet.getInt("UniversityID"));
                    }

                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }


        return  result;
    }

    public static List<University>getUniversities(){
        List<University>result=new ArrayList<University>();
        try (Connection connection= DB.getConnection()){
            try(Statement statement=connection.createStatement()){
                try (ResultSet resultSet=statement.executeQuery("SELECT * FROM university")){
                    while(resultSet.next()){
                        University uni=new University();
                        uni.setUniversityName(resultSet.getString("universityname"));
                        uni.setUniversityId(resultSet.getInt("UniversityID"));
                        result.add(uni);
                    }
                }

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }


        return  result;

    }
    public static boolean deleteUniversity(int id){
        try (Connection connection= DB.getConnection()){
            try(PreparedStatement statement=connection.prepareStatement("DELETE FROM university WHERE universityid =?")){
                statement.setInt(1,id);
                int result=statement.executeUpdate();
                if(result>0)
                    return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return  false;
    }

}
