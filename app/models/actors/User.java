package models.actors;

import models.entities.Message;
import play.db.DB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by kozo on 6/14/16.
 */
public abstract class User {
    private String userName;
    private  String name;
    private String surname;
    private  String email;
    private  String password;
    private Date birthday;
    private String birthPlace;
    private String profilepic;
    private String bio;
    private boolean status;
    private Date    date;

    public String getUserName() {
        return userName;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public Date getBirthday() {
        return birthday;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public String getProfilepic() {
        return profilepic;
    }

    public String getBio() {
        return bio;
    }

    public boolean isStatus() {
        return status;
    }

    public Date getDate() {
        return date;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public void setProfilepic(String profilepic) {
        this.profilepic = profilepic;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    User (ResultSet set) throws SQLException {
        if (set==null)
            return;
        name=(set.getString("name"));
        userName=set.getString("username");
        password=set.getString("password");
        surname=set.getString("surname");
        email=set.getString("email");
        birthday=set.getDate("birthday");
        bio=set.getString("bio");
        profilepic=set.getString("profilepicture");
        date=set.getDate("date");
        status=set.getBoolean("status");
        birthPlace=set.getString("birthplace");
    }
    public  String genrateToken(){

        return  java.util.UUID.randomUUID().toString();
    }
    public  abstract boolean update();

    public  boolean sentMessageTo(String reciverName, String messageSubject, String message){
        boolean result=false;
        String sql="INSERT INTO message( susername, rusername, subject, body, status, sentdate) " +
                "VALUES (?,?,?,?,?,?)";
        try (Connection con= DB.getConnection()){
            try (PreparedStatement statement=con.prepareStatement(sql)){
                int i=1;
                statement.setString(i++,getUserName());
                statement.setString(i++,reciverName);
                statement.setString(i++,messageSubject);
                statement.setString(i++,message);
                statement.setBoolean(i++,true);
                statement.setDate(i++,new java.sql.Date(new Date().getTime()));
                result=statement.executeUpdate()>0;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return  result;
    }

    public List<Message> getSentEmails() {
        List<Message> result=new ArrayList<>();
        try(Connection con=DB.getConnection() ) {
            try(PreparedStatement statement=con.prepareStatement(
                    "SELECT * FROM message WHERE susername=? ")){
                statement.setString(1,userName);
                try(ResultSet set=statement.executeQuery()){
                    while(set.next()){
                        result.add(new Message(set));
                    }
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    public List<Message> getInbox() {
        List<Message> result=new ArrayList<>();
        try(Connection con=DB.getConnection() ) {
            try(PreparedStatement statement=con.prepareStatement(
                    "SELECT * FROM message WHERE rusername=? ")){
                statement.setString(1,userName);
                try(ResultSet set=statement.executeQuery()){
                    while(set.next()){
                        result.add(new Message(set));
                    }
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;

    }
    public User(){}
}
