package models.actors;

import models.StudentManager;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by kozo on 6/18/16.
 */
public class Admin extends User{
    public Admin(ResultSet set) throws SQLException {
        super(set);
    }

    public  Admin() throws SQLException {
        super(null);
    }

    @Override
    public boolean update() {
        return false;
    }


    public boolean changeStudentStatus(String studentUserName,boolean status) {
        boolean result=false;
        Student student= StudentManager.getStudentByUserName(studentUserName);
        student.setStatus(status);

        return student.update();

    }
}
