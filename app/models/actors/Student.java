package models.actors;


import models.helpers.Password;
import play.db.DB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * Created by kozo on 6/10/16.
 */

////////// STUDENT MODEL ///////////////
public class Student extends User {
    private int universityID;
    private  String studentID;

    private int    rank;

    public int getUniversityID() {
        return universityID;
    }

    public String getStudentID() {
        return studentID;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public  boolean update(){
        boolean result=false;
        String sql="UPDATE student SET " +
                "bio=?," +
                "name=?," +
                "surname=?," +
                "birthday=?," +
                "birthplace=?," +
                "status=?," +
                "password=?," +
                "universityid=?," +
                "studentid=?," +
                "rank=?," +
                "email=?," +
                "date =?," +
                "profilepicture=?" +
                "WHERE username=?" ;
        try (Connection connection= DB.getConnection()){
            try (PreparedStatement statement=connection.prepareStatement(sql)){
                statement.setString(1,getBio());
                statement.setString(2,getName());
                statement.setString(3,getSurname());
                java.sql.Date vo=new java.sql.Date(getBirthday().getTime());
                statement.setDate(4,vo );
                statement.setString(5,getBirthPlace());
                statement.setBoolean(6,isStatus());
                statement.setString(7,getPassword());
                statement.setInt(8, universityID);
                statement.setString(9,studentID);
                statement.setInt(10,rank);
                statement.setString(11,getEmail());
                statement.setDate(12,new java.sql.Date(getDate().getTime()));
                statement.setString(13,getProfilepic());
                statement.setString(14,getUserName());
                result=statement.executeUpdate()>0;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
//
    public Student(ResultSet set) throws SQLException {
        super(set);
        universityID =set.getInt("universityid");
        studentID=set.getString("studentid");
        rank=set.getInt("rank");
    }


    public Student() throws SQLException {
        super(null);
    }

}
