package models.entities;

import play.db.DB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by SezerToprak on 19.06.2016.
 */
public class Note {
    private int noteId;
    private String userName;
    private int courseUniDepId;
    private String title;
    private String text;
    private boolean state;
    private Date date;
    private int upVoteCounter;
    private int downVoteCounter;
    HashMap<String, Boolean> voters;
    public Map<String, Boolean> getVoters() {
        return Collections.unmodifiableMap(voters);
    }




    public int getUpVoteCounter() {
        return upVoteCounter;
    }


    public int getDownVoteCounter() {
        return downVoteCounter;
    }

    public int getNodeId() {
        return noteId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getCourseUniDepId() {
        return courseUniDepId;
    }

    public void setCourseUniDepId(int courseUniDepId) {
        this.courseUniDepId = courseUniDepId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean getState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }


    public Note() {
        voters = new HashMap<>();

    }

    public Note(ResultSet set) throws SQLException {
        if (set == null)
            return;

        noteId = set.getInt("noteid");
        userName = set.getString("username");
        courseUniDepId = set.getInt("courseunidepid");
        title = set.getString("title");
        text = set.getString("text");
        state = set.getBoolean("state");
        date = set.getDate("date");
        upVoteCounter = set.getInt("upvote");
        downVoteCounter = set.getInt("downvote");
        voters = new HashMap<>();

        getUpVotersDownVotersFromDataBase();
    }

    private void getUpVotersDownVotersFromDataBase() {
        try (Connection con = DB.getConnection()) {
            String sql = "SELECT username,votetype FROM votes WHERE noteid=?";
            try (PreparedStatement statement = con.prepareStatement(sql)) {
                statement.setInt(1, noteId);
                try (ResultSet set = statement.executeQuery()) {
                    while (set.next()) {
                        if (set.getBoolean("votetype") == true) {
                            voters.put(set.getString("username"), true);
                        } else
                            voters.put(set.getString("username"), false);
                    }
                }

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void upVoteBy(String userName) {
        if (voters.containsKey(userName)) {
            if (!voters.get(userName)) {
                voters.put(userName, true);
                downVoteCounter--;
                upVoteCounter++;
                updateDataBaseVote(userName,true);
            }

        } else {
            voters.put(userName, true);
            upVoteCounter++;
            updateDataBaseVote(userName,true);
        }


    }

    private void updateDataBaseVote(String userName, Boolean voteType) {
        String sql = "SELECT \"vote\"(?,?,?)";
        try (Connection con = DB.getConnection()) {
            try (PreparedStatement statement = con.prepareStatement(sql)) {
                statement.setString(1, userName);
                statement.setInt(2, noteId);
                statement.setBoolean(3, voteType);
                statement.execute();
                statement.execute();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void downVoteBy(String UserName) {
        if (voters.containsKey(UserName)) {
            if (voters.get(UserName)) {
                voters.put(UserName, false);
                downVoteCounter++;
                upVoteCounter--;
                updateDataBaseVote(UserName,false);
            }

        } else {
            voters.put(UserName, false);
            downVoteCounter++;
            updateDataBaseVote(UserName,false);
        }


    }
}