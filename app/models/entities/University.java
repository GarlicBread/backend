package models.entities;

/**
 * Created by kozo on 6/10/16.
 */
public class University {
    private int universityId;
    private String universityName;

    public int getUniversityId() {
        return universityId;
    }

    public void setUniversityId(int universityId) {
        this.universityId = universityId;
    }

    public String getUniversityName() {
        return universityName;
    }

    public void setUniversityName(String universityName) {
        this.universityName = universityName;
    }
}
