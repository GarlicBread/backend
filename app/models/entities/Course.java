package models.entities;

/**
 * Created by SezerToprak on 15.06.2016.
 */
public class Course {
    private int courseId;
    private String courseName;

    public int getCourseId(){return courseId;}
    public void setCourseId(int courseId){this.courseId = courseId;}

    public String getCourseName(){return courseName;}
    public void setCourseName(String courseName){this.courseName = courseName;}

}
