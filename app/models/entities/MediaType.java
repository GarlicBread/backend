package models.entities;

/**
 * Created by SezerToprak on 20.06.2016.
 */
public class MediaType {
    private int mediaTypeId;
    private String typeName;

    public int getMediaTypeId(){return mediaTypeId;}
    public void setMediaTypeId(int mediaTypeId){this.mediaTypeId = mediaTypeId;}

    public String getTypeName(){return typeName;}
    public void setTypeName(String typeName){this.typeName = typeName;}
}
