package models.entities;

/**
 * Created by SezerToprak on 15.06.2016.
 */
public class Department {
    private int departmentId;
    private String departmentName;

    public int getDepartmentId(){
        return departmentId;
    }
    public void setDepartmentId(int departmentId){
        this.departmentId = departmentId;
    }

    public String getDepartmentName(){
        return departmentName;
    }
    public void setDepartmentName(String departmentName){
        this.departmentName=departmentName;
    }
}
