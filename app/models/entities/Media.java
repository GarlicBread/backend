package models.entities;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by SezerToprak on 20.06.2016.
 */
public class Media {
    private int mediaId;
    private int noteId;
    private int mediaTypeId;
    private String mediaTitle;
    private String mediaPath;

    public int getMediaId(){return mediaId;}
    public void setMediaId(int mediaId){this.mediaId=mediaId;}

    public int getNoteId(){return noteId;}
    public void setNoteId(int noteId){this.noteId = noteId;}

    public int getMediaTypeId(){return mediaTypeId;}
    public void setMediaTypeId(int mediaTypeId){this.mediaTypeId = mediaTypeId;}

    public String getMediaTitle(){return mediaTitle;}
    public void setMediaTitle(String mediaTitle){this.mediaTitle = mediaTitle;}

    public String getMediaPath(){return mediaPath;}
    public void setMediaPath(String mediaPath){this.mediaPath = mediaPath;}

    public Media(){}

    public Media(ResultSet set)throws SQLException{
        if (set==null)
            return;

        mediaId = set.getInt("mediaid");
        noteId = set.getInt("noteid");
        mediaTypeId = set.getInt("mediatypeid");
        mediaTitle = set.getString("mediatitle");
        mediaPath = set.getString("mediapath");
    }
}
