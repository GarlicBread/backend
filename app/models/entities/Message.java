package models.entities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * Created by kozo on 6/19/16.
 */
public class Message {
    private  int id;
    private  String senderUserName;
    private String  reciverUserName;
    private String  subject;
    private String body;
    boolean visible;
    Date    sentDate;


    public Date getSentDate() {
        return sentDate;
    }
    public int getID() {
        return id;
    }

    public String getSenderUserName() {
        return senderUserName;
    }

    public String getReciverUserName() {
        return reciverUserName;
    }

    public String getSubject() {
        return subject;
    }

    public String getBody() {
        return body;
    }

    public boolean isVisible() {
        return visible;
    }

    public Message(ResultSet set) throws SQLException {
        id=set.getInt("messageid");
        senderUserName=set.getString("susername");
        reciverUserName=set.getString("rusername");
        subject=set.getString("subject");
        body=set.getString("body");
        visible=set.getBoolean("status");
        sentDate=set.getDate("sentdate");
    }
}
