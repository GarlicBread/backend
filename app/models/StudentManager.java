package models;

import models.actors.Student;
import models.helpers.Password;
import play.db.DB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * Created by kozo on 6/20/16.
 */
public class StudentManager {

    public static Student getStudentByUserNameAndPassword(String userName, String password) {
        try(Connection c= DB.getConnection()){
            try (PreparedStatement statement=c.prepareStatement("SELECT * FROM student WHERE username=?")){
                statement.setString(1,userName);
                try (ResultSet resultSet=statement.executeQuery()){
                    if(resultSet.next()){
                        String _password=resultSet.getString("password");
                        if(Password.check(password,_password)){
                           return  new Student(resultSet);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean addStudent(String userName, String password, String name, String surname, int universityID,
                                     String studentID, String email,
                                     Date birthday, String birthPlace,
                                     String bio, int rank, boolean status, Date date) {
        String sqlInsert="INSERT INTO student (username, studentid, universityid, name, surname, email, birthday" +
                ", birthplace, bio, password, rank, status, date) " +
                "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
        try(Connection connection= DB.getConnection()){

            try (PreparedStatement statement=connection.prepareStatement(sqlInsert)){

                statement.setString(1,userName);
                statement.setString(2,studentID);
                statement.setInt(3,universityID);
                statement.setString(4,name);
                statement.setString(5,surname);
                statement.setString(6,email);
                statement.setDate(7,new java.sql.Date(birthday.getTime()));
                statement.setString(8,birthPlace);
                statement.setString(9,bio);
                String hashedPassowrd= Password.getSaltedHash(password);
                statement.setString(10,hashedPassowrd);
                statement.setInt(11,rank);
                statement.setBoolean(12,status);
                statement.setDate(13,new java.sql.Date(date.getTime()));


                if(statement.executeUpdate()>0){
                    return true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return  false;
    }

    public static boolean addStudent(Student student){
        boolean result=addStudent(student.getUserName(),student.getPassword(),student.getName(),student.getSurname()
                ,student.getUniversityID(),student.getStudentID(),student.getEmail(),student.getBirthday(),student.getBirthPlace(),student.getBio()
                ,student.getRank(),true,student.getDate());
        return result;
    }

    public static Student getStudentByUserName(String studentUserName) {
        Student result=null;

        try (Connection con=DB.getConnection()){
            try (PreparedStatement statement=con.prepareStatement("SELECT  * FROM student WHERE username=?")){
                statement.setString(1,studentUserName);
                try (ResultSet set=statement.executeQuery()){
                    if(set.next()){
                        result=new Student(set);
                    }

                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return  result;

    }
}
