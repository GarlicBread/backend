package models;

import models.entities.MediaType;
import play.db.DB;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by SezerToprak on 20.06.2016.
 */
public class MediaTypeManager {
    public static boolean addMediaType(String typeName){
        Connection c = DB.getConnection();
        PreparedStatement statement = null;

        try{
            statement = c.prepareStatement("INSERT  INTO mediatype(typename) VALUES (?)");
            statement.setString(1,typeName);
            int result = statement.executeUpdate();
            if(result>0)
                return true;

        }catch (SQLException e){
            e.printStackTrace();
        }
        return false;
    }

    public boolean addMediaType(MediaType mediaType){
        return addMediaType(mediaType.getTypeName());
    }

    public static MediaType getMediaType(int mediaTypeId){
        MediaType result = null;

        try(Connection c=DB.getConnection()){
            try(PreparedStatement s=c.prepareStatement("SELECT * FROM mediatype WHERE mediatypeid =?")){
                s.setInt(1,mediaTypeId);

                try(ResultSet resultSet=s.executeQuery()){
                    if (resultSet.next()){
                        result=new MediaType();
                        result.setMediaTypeId(resultSet.getInt("mediatypeid"));
                        result.setTypeName(resultSet.getString("typename"));
                    }

                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    public static List<MediaType> getMediaTypes(){
        List<MediaType> result  = new ArrayList<MediaType>();

        try (Connection connection= DB.getConnection()){
            try(Statement statement=connection.createStatement()){
                try (ResultSet resultSet=statement.executeQuery("SELECT * FROM mediatype")){
                    while(resultSet.next()){
                        MediaType mediatype=new MediaType();
                        mediatype.setMediaTypeId(resultSet.getInt("mediatypeid"));
                        mediatype.setTypeName(resultSet.getString("typename"));
                        result.add(mediatype);
                    }
                }

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static boolean deleteMediaTypeId(int mediaTypeId){
        try (Connection connection= DB.getConnection()){
            try(PreparedStatement statement=connection.prepareStatement("DELETE FROM mediatype WHERE mediatypeid =?")){
                statement.setInt(1,mediaTypeId);
                int result=statement.executeUpdate();
                if(result>0)
                    return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return  false;
    }
}
