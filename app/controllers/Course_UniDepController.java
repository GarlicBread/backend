package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import models.*;
import models.relations.Course_UniDep;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import java.io.IOException;
import java.util.List;

/**
 * Created by SezerToprak on 16.06.2016.
 */
public class Course_UniDepController extends Controller {

    @BodyParser.Of(BodyParser.Json.class)
    public Result addCourseUniDep(){
        JsonNode request=request().body().asJson();
        ObjectMapper mapper = Json.newDefaultMapper();
//                .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
//                .disable(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES)
//                .disable(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES);
        try {
            Course_UniDep courseunidep = mapper.readValue(request.toString(), Course_UniDep.class);
            if(Course_UniDepManager.addCourseUniDep(courseunidep)){
                return created();
            }
        }
        catch (IOException e){
            e.printStackTrace();
        }
        return  status(409,"conflict");//conflict
    }

    public Result getCourseUniDep(int uniDepId){
        Course_UniDep courseunidep = Course_UniDepManager.getCourseUniDep(uniDepId);
        if(courseunidep == null)
            return notFound();
        JsonNode json = Json.toJson(courseunidep);
        return ok(json);
    }

    public Result getCourseUniDeps(){
        List<Course_UniDep> result = Course_UniDepManager.getCourseUniDeps();
        JsonNode json = Json.toJson(result);
        return ok(json);
    }

    public Result deleteCourseUniDep(int courseUniDepId){
        if(Course_UniDepManager.deleteCourseUniDep(courseUniDepId))
            return ok();

        return notFound();
    }
}
