package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import models.AdminManager;
import models.NoteManager;
import models.StudentManager;
import models.actors.User;
import models.entities.Note;
import play.api.cache.CacheApi;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Result;
import scala.concurrent.duration.Duration;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

/**
 * Created by kozo on 6/18/16.
 */
public class UserController extends AuthorizedController {
    public static final String profilePicPath = "public/profilepictures/";
    public static final String postPdfFilePath = "public/pdf/";

    @Inject
    CacheApi cacheApi;

    public Result login() {
        JsonNode node = request().body().asJson();
        String userName = node.findPath("userName").asText();
        String password = node.findPath("password").asText();
        User loginResult = StudentManager.getStudentByUserNameAndPassword(userName, password);//try to log in as Student;
        if (loginResult == null) {
            //it's not a student so try to login as admin
            loginResult = AdminManager.getAdminByUserNameAndPassword(userName, password);
            if (loginResult == null) {
                return unauthorized();// not a Admin then return unauthorized
            }
        }
        String token = loginResult.genrateToken();
        HashMap<String, String> result = new HashMap<>();
        result.put("token", token);
        cacheApi.set(token, loginResult, Duration.apply(60, TimeUnit.MINUTES));
        return ok(Json.toJson(result));
    }

    //todo delete the old pic
    public Result uploadPic() throws IOException {
        String token = request().getHeader("token");
        User user;
        if ((user = getUserByToken(token)) != null) {
            File pic = (File) request().body().asMultipartFormData().getFiles().get(0).getFile();
            String fileName = org.apache.commons.lang3.RandomStringUtils.random(10, "abcdefdgaawqewqe12345908841");
            File output = new File(profilePicPath + fileName + ".png");
            FileOutputStream ou = null;
            try {
                ou = new FileOutputStream(output);
                FileInputStream io = new FileInputStream(pic);
                ou.getChannel().transferFrom(io.getChannel(), 0, io.getChannel().size());
                user.setProfilepic(profilePicPath + fileName + ".png");
                if (!user.update()) {
                    return internalServerError();
                }
                return ok();
            } catch (IOException e) {
                e.printStackTrace();
                return internalServerError();
            } finally {
                ou.getChannel().close();


            }

        }
        return unauthorized();
    }


    @BodyParser.Of(BodyParser.Json.class)
    public Result post() {
        String token = request().getHeader("token");
        User user;

        if ((user = getUserByToken(token)) != null) {
            JsonNode request=request().body().asJson();
            ObjectMapper mapper = Json.newDefaultMapper();
//                .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
//                .disable(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES)
//                .disable(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES);

            try {
                Note note=mapper.readValue(request.toString(), Note.class);
                //add the note
                if(NoteManager.addNote(note)){
                    //add media to the note
                    if(NoteManager.addMediaToNote(note,postPdfFilePath));
                    return created();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return status(409);//conflict
        }

        return unauthorized();
    }
}
