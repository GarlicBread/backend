package controllers;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import models.StudentManager;
import models.actors.Student;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Result;

import java.io.IOException;

/**
 * Created by kozo on 6/12/16.
 */
public class StudentController extends AuthorizedController {




    @BodyParser.Of(BodyParser.Json.class)
    public  Result addStudent(){
        JsonNode request=request().body().asJson();
        ObjectMapper mapper = Json.newDefaultMapper();
//                .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
//                .disable(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES)
//                .disable(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES);

        try {
            Student student=mapper.readValue(request.toString(), Student.class);
            boolean result= StudentManager.addStudent(student);
            if(result )
                return created();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return status(409);//conflict

    }





}
