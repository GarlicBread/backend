package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import models.entities.University;
import models.UniversityManager;
import models.actors.Admin;
import models.actors.User;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Result;

import java.io.IOException;
import java.util.List;

/**
 * Created by kozo on 6/10/16.
 */
public class UniversityController extends AuthorizedController {



    public Result getUniversity(int id) {
        University university=UniversityManager.getUniversity(id);
        if(university==null)
            return notFound();
        JsonNode json= Json.toJson(university);
        return ok(json);
    }
    public Result getUniversties() {
        List<University> universities=UniversityManager.getUniversities();
        JsonNode json= Json.toJson(universities);
        return ok(json);
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result addUniversity(){
        User user=this.getUserByToken(request().getHeader("token"));
        if(!(user instanceof Admin)){
            return  unauthorized();
        }
        JsonNode request=request().body().asJson();
        ObjectMapper mapper = Json.newDefaultMapper();
//                .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
//                .disable(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES)
//                .disable(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES);
        try {
            University uni = mapper.readValue(request.toString(), University.class);
            if (UniversityManager.addUniversity(uni)) {
                return created();
            }
        }
        catch (IOException e){
            e.printStackTrace();
        }
        return  status(409,"conflict");//conflict
    }

    public Result deleteUniversity(int id){
        if(UniversityManager.deleteUniversity(id)){
            return ok();
        }
        return notFound();
    }

}
