package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import models.relations.Uni_Dep;
import models.Uni_DepManager;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import java.io.IOException;
import java.util.List;

/**
 * Created by SezerToprak on 16.06.2016.
 */
public class Uni_DepController extends Controller {

    @BodyParser.Of(BodyParser.Json.class)
    public Result addUniDep(){
        JsonNode request=request().body().asJson();
        ObjectMapper mapper = Json.newDefaultMapper();
//                .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
//                .disable(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES)
//                .disable(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES);
        try {
            Uni_Dep unidep = mapper.readValue(request.toString(), Uni_Dep.class);
            if(Uni_DepManager.addUniDep(unidep)){
                return created();
            }
        }
        catch (IOException e){
            e.printStackTrace();
        }
        return  status(409,"conflict");//conflict
    }

    public Result getUniDep(int uniDepId){
        Uni_Dep unidep = Uni_DepManager.getUniDep(uniDepId);
        if(unidep == null){
            return notFound();
        }
        JsonNode json = Json.toJson(unidep);
        return ok(json);
    }

    public Result getUniDeps(){
        List<Uni_Dep> result = Uni_DepManager.getUniDeps();
        JsonNode json = Json.toJson(result);
        return ok(json);
    }

    public Result deleteUniDep(int uniDepId){
        if(Uni_DepManager.deleteUniDep(uniDepId))
            return ok();

        return notFound();
    }
}
