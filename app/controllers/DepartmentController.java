package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import models.entities.Department;
import models.DepartmentManager;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import java.io.IOException;
import java.util.List;

/**
 * Created by SezerToprak on 15.06.2016.
 */
public class DepartmentController extends Controller {

    public Result getDepartment(int id){
        Department department = DepartmentManager.getDepartment(id);
        if(department==null){
            return notFound();
        }
        JsonNode json = Json.toJson(department);
        return ok(json);
    }

    public Result getDepartments(){
        List<Department> departments = DepartmentManager.getDepartments();
        JsonNode json = Json.toJson(departments);
        return ok(json);
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result addDepartment(){
        JsonNode request=request().body().asJson();
        ObjectMapper mapper = Json.newDefaultMapper();
//                .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
//                .disable(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES)
//                .disable(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES);
        try {
            Department dep = mapper.readValue(request.toString(), Department.class);
            if(DepartmentManager.addDepartment(dep)){
                return created();
            }
        }
        catch (IOException e){
            e.printStackTrace();
        }
        return  status(409,"conflict");//conflict
    }

    public Result deleteDepartment(int departmentId){
        if(DepartmentManager.deleteDepartment(departmentId)){
            return ok();
        }
        return notFound();
    }
}
