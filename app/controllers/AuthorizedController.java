package controllers;

import com.google.inject.Inject;
import models.actors.User;

import play.cache.*;
import play.mvc.Controller;

/**
 * Created by kozo on 6/14/16.
 */
public class AuthorizedController extends Controller{
    @Inject
    CacheApi cache;
    public User getUserByToken(String token){
       User user=null;

        Object o=cache.get(token);
        if(o !=null && o instanceof User){
            user= (User) o;
        }
        return  user;
    }
}
