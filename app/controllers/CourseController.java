package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import models.entities.Course;
import models.CourseManager;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import java.io.IOException;
import java.util.List;

/**
 * Created by SezerToprak on 15.06.2016.
 */
public class CourseController extends Controller {

    @BodyParser.Of(BodyParser.Json.class)
    public Result addCourse(){
        JsonNode request=request().body().asJson();
        ObjectMapper mapper = Json.newDefaultMapper();
//                .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
//                .disable(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES)
//                .disable(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES);
        try {
            Course course = mapper.readValue(request.toString(), Course.class);
            if(CourseManager.addCourse(course)){
                return created();
            }
        }
        catch (IOException e){
            e.printStackTrace();
        }
        return  status(409,"conflict");//conflict
    }

    public Result getCourse(int courseId){
        Course course = CourseManager.getCourse(courseId);
        if(course==null){
            return notFound();
        }
        JsonNode json = Json.toJson(course);
        return ok(json);
    }

    public Result getCourses(){
        List<Course> course = CourseManager.getCourses();
        JsonNode json = Json.toJson(course);
        return ok(json);
    }

    public Result deleteCourse(int courseId){
        if(CourseManager.deleteCourse(courseId)){
            return ok();
        }
        return notFound();
    }
}
