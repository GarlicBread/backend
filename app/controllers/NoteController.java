package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import models.NoteManager;
import models.entities.Note;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import java.io.IOException;
import java.util.List;

/**
 * Created by SezerToprak on 19.06.2016.
 */
public class NoteController extends Controller {

    @BodyParser.Of(BodyParser.Json.class)
    public Result addNote(){
        JsonNode request=request().body().asJson();
        ObjectMapper mapper = Json.newDefaultMapper();
//                .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
//                .disable(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES)
//                .disable(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES);

        try {
            Note note=mapper.readValue(request.toString(), Note.class);
            boolean result= NoteManager.addNote(note);
            if(result )
                return created();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return status(409);//conflict
    }

    public Result getNote(int noteId){
        Note note = NoteManager.getNote(noteId);
        if(note==null)
            return notFound();

        JsonNode json = Json.toJson(note);
        return ok(json);
    }

    public Result getNotes(){
        List<Note> result = NoteManager.getNotes();
        JsonNode json = Json.toJson(result);
        return ok(json);
    }

    public Result deleteNote(int noteId){
        if(NoteManager.deleteNote(noteId))
            return ok();

        return notFound();
    }
}
