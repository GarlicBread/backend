package tests.controllers;

import models.AdminManager;
import models.StudentManager;
import models.UniversityManager;
import models.actors.Admin;
import models.actors.Student;
import models.actors.User;
import org.junit.Test;
import play.api.cache.Cache;
import tests.TestWithDataBase;
import tests.tools.RestClient;

import java.util.Calendar;

import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;
import static play.test.Helpers.running;

/**
 * Created by kozo on 6/18/16.
 */
public class UserControllerTest  extends TestWithDataBase {

    @Test
    public void testStudentLogin() throws  Exception{
        String username="lucey";
        String password="pasda!#125";
        running( fakeApp,()->{
            UniversityManager.addUniversity("iku");
            int uniId= UniversityManager.getUniversities().get(0).getUniversityId();
            StudentManager.addStudent(username,
                    password,"omar","zahed",uniId,"12345","py4object@gmial.com",
                    new java.util.Date(2000, Calendar.JANUARY,3),"NYc","i love pizza",
                    0,true,new java.util.Date());

            RestClient restClient=new RestClient(fakeApp.port());
            try {
                boolean loginResult=restClient.login(username,password);
                assertTrue(loginResult);
                User user= (User) Cache.get(restClient.getToken(),fakeApp.application()).get();
                assertTrue(user instanceof  Student);


            } catch (Exception e) {
                fail();
            }

        });
    }
    @Test
    public void testLoginAdmin(){
        String username="lucey";
        String password="pasda!#125";
        running( fakeApp,()->{
            AdminManager.add(username,
                    password,"omar","zahed","py4object@gmial.com",
                    new java.util.Date(2000, Calendar.JANUARY,3),"NYc","i love pizza",true,new java.util.Date());
            RestClient restClient=new RestClient(fakeApp.port());
            try {
                boolean loginResult=restClient.login(username,password);
                assertTrue(loginResult);
                User user= (User) Cache.get(restClient.getToken(),fakeApp.application()).get();
                assertTrue(user instanceof Admin);


            } catch (Exception e) {
                fail();
            }

        });
    }


}
