package tests.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.exceptions.UnirestException;
import models.*;
import models.entities.Note;
import org.junit.Test;
import play.libs.Json;
import tests.TestWithDataBase;
import tests.tools.RestClient;

import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static play.test.Helpers.running;

/**
 * Created by SezerToprak on 19.06.2016.
 */
public class PostControllerTest extends TestWithDataBase {

    @Test
    public void addPostWithoutLoggingAttachMedia(){
        running(fakeApp,()->{
            RestClient client = new RestClient(fakeApp.port());
            try {
                DepartmentManager.addDepartment("Industriel Engineering");
                UniversityManager.addUniversity("IKU");

                int uniId=UniversityManager.getUniversities().get(0).getUniversityId();
                int depId=DepartmentManager.getDepartments().get(0).getDepartmentId();
                Uni_DepManager.addUniDep(uniId,depId);

                int unidepId = Uni_DepManager.getUniDeps().get(0).getUniDepId();
                CourseManager.addCourse("Physics");
                int courseId=CourseManager.getCourses().get(0).getCourseId();
                Course_UniDepManager.addCourseUniDep(unidepId,courseId);
                int coursunidepid= Course_UniDepManager.getCourseUniDeps().get(0).getCourseUniDepId();

                StudentManager.addStudent("tprkszr",
                        "12345","sezer","toprak",uniId,"12345","tprkszr@gmial.com",
                        new java.util.Date(2000, Calendar.JANUARY,3),"NYc","i love pizza too",
                        0,true,new java.util.Date());


                Note note = new Note();
                note.setUserName("tprkszr");
                note.setCourseUniDepId(coursunidepid);
                note.setTitle("title");
                note.setText("text");
                note.setState(true);
                note.setDate(new Date());
                JsonNode json = Json.toJson(note);
                //System.out.println(json.toString());
                HttpResponse<String> response=
                        client.postWithoutTokenJsonBody("/post/",json.toString());

                MediaTypeManager.addMediaType("Video");
                int mediaTypeId = MediaTypeManager.getMediaTypes().get(0).getMediaTypeId();
                int noteId = NoteManager.getNotes().get(0).getNodeId();
                MediaManager.addMedia(noteId,mediaTypeId,"Funny cat video","Path");
                // test insertion queries for university-department-unidep--course-courseunidep
                assertTrue(UniversityManager.getUniversities().size()==1);
                assertTrue(DepartmentManager.getDepartments().size()==1);
                assertTrue(Uni_DepManager.getUniDeps().size()==1);
                assertTrue(CourseManager.getCourses().size()==1);
                assertTrue(Course_UniDepManager.getCourseUniDeps().size()==1);
                // post a note
                assertTrue(response.getStatus()==201 );//concflict
                assertTrue(NoteManager.getNotes().size()==1);
                // add media to the note
                assertTrue(MediaTypeManager.getMediaTypes().size()==1);
                assertTrue(MediaManager.getAllMedia().size()==1);

            } catch (UnirestException e) {
                e.printStackTrace();
                fail();
            }
        });
    }
}
