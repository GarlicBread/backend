package tests.controllers;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.exceptions.UnirestException;
import models.AdminManager;
import models.StudentManager;
import models.entities.University;
import models.UniversityManager;
import org.junit.Test;
import tests.TestWithDataBase;
import tests.tools.RestClient;

import java.util.Calendar;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static play.test.Helpers.running;

/**
 * Created by kozo on 6/19/16.
 */
public class UniversityControllerTest extends TestWithDataBase{

    @Test
    public  void testAddUniversityWithoutLogIn(){

        running(fakeApp,()->{
            RestClient client=new RestClient(fakeApp.port());
            try {
                HttpResponse<String> response=
                        client.postWithoutTokenJsonBody("/university/",
                                "{\"universityName\":\"iku\"}");
                assertTrue(response.getStatus()==401 );//unauthorized
                assertTrue(UniversityManager.getUniversities().size()==0);

            } catch (UnirestException e) {
                e.printStackTrace();
                fail();
            }


        });

    }

    @Test
    public  void  testAddingUniveristyWithStudent() {
        String userName = "ascasdasx", password = "fisadmsa12321";
        String univeristyName = "sasdcax";
        running(fakeApp, () -> {
            UniversityManager.addUniversity("iku");
            int uniId = UniversityManager.getUniversities().get(0).getUniversityId();
            StudentManager.addStudent(userName,
                    password, "omar", "zahed", uniId, "12345", "py4object@gmial.com",
                    new java.util.Date(2000, Calendar.JANUARY, 3), "NYc", "i love pizza",
                    0, true, new java.util.Date());
            RestClient restClient = new RestClient(fakeApp.port());
            try {
                restClient.login(userName, password);
            } catch (Exception e) {
                e.printStackTrace();
                fail();
            }
            try {
                HttpResponse<String> response = restClient.postWithTokenJsonBody("/university/",
                        "{\"universityName\":\""+univeristyName+"\"}");
                assertTrue(response.getStatus() == 401);//unauthorized

                assertTrue(UniversityManager.getUniversities().size()==1);


            } catch (UnirestException e) {
                e.printStackTrace();
                fail();
            }
        });
    }

    @Test
    public  void  testAddingUniveristyWithAdmin(){
        String userName="ascasdasx",password="fisadmsa12321";
        String univeristyName="sasdcax";
        running(fakeApp,()->{

            AdminManager.add(userName,
                    password,"omar","zahed","py4object@gmial.com",
                    new java.util.Date(2000, Calendar.JANUARY,3),"NYc","i love pizza",
                    true,new java.util.Date());
            RestClient restClient=new RestClient(fakeApp.port());
            try {
                restClient.login(userName,password);
            } catch (Exception e) {
                e.printStackTrace();
                fail();
            }
            try {
                HttpResponse<String> response=restClient.postWithTokenJsonBody("/university/",
                        "{\"universityName\":\""+univeristyName+"\"}");
                assertTrue(response.getStatus()==201);//created
                University uni=UniversityManager.getUniversities().get(0);
                assertTrue(UniversityManager.getUniversities().size()==1);
                assertTrue(uni.getUniversityName().equals(univeristyName));

            } catch (UnirestException e) {
                e.printStackTrace();
                fail();
            }
        });

    }

}
