package tests;

import org.junit.After;
import org.junit.Before;
import tests.tools.Tools;

import static play.test.Helpers.testServer;

/**
 * Created by kozo on 6/18/16.
 */
public  abstract class TestWithDataBase {


    protected  play.test.TestServer fakeApp;
    @Before
    public  void setUp(){
        fakeApp = testServer(9009);
        Tools.eraseDataBase();

    }
    @After
    public  void after(){

    }

}
