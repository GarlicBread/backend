package tests.tools;

import com.google.common.collect.ImmutableMap;
import models.AdminManager;
import models.StudentManager;
import models.UniversityManager;
import play.db.Database;
import play.db.Databases;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by kozo on 6/17/16.
 */
public class Tools {
    public  static Database db = Databases.createFrom(
            "test3",
            "org.postgresql.Driver",
            "jdbc:postgresql://localhost:5432/test3",
            ImmutableMap.of(
                    "username", "postgres",
                    "password", "1234"
            )
    );

    public static String getNameOfDataBase(){

        try (Connection connection= db.getConnection()){
            try(Statement statement=connection.createStatement()){
                try (ResultSet set=statement.executeQuery("SELECT  current_database()")){
                    if(set.next()){
                        return set.getString(1);
                    }

                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void eraseDataBase(){
        String name=getNameOfDataBase();
        if(name.equals("test3")){
            try (Connection con=db.getConnection()){
                try (Statement s=con.createStatement()){
                    s.execute("SELECT * FROM  truncate_tables()");
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public  static void AddAdmin(String userName,String password){
        AdminManager.add(userName,password,"dasdcasw","wqrfaxc",
                "afsad@fasf.com",new  java.util.Date(2000, Calendar.JANUARY,3),"NYC","ascas",true,new Date());
    }

    public static void AddStudent(String userNamem,String password){
        UniversityManager.addUniversity("iku");
        int uniId= UniversityManager.getUniversities().get(0).getUniversityId();
        StudentManager.addStudent(userNamem,
                password,"omar","zahed",uniId,"12345","py4object@gmial.com",
                new java.util.Date(2000, Calendar.JANUARY,3),"NYc","i love pizza",
                0,true,new java.util.Date());
    }

}
