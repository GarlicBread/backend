package tests.tools;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

/**
 * Created by kozo on 6/16/16.
 * This class will be used to connect to the server makes some requests
 * and get responses
 *
 */
public class RestClient {
    final String _baseUrl="http://127.0.0.1:9000";
    private final String login="/login";

    String token;
    String baseUrl;
    public  boolean login(String userName,String password) throws Exception {
        HttpResponse<JsonNode> response = Unirest.post(baseUrl + login).
                header("Content-type", "application/json").
                body(
                        "{\"userName\":\"" + userName + "\",\"password\":\"" + password + "\"}")
                .asJson();
        if(response.getStatus()!=200){
            return  false;
        }
        token=response.getBody().getObject().get("token").toString();
        if(token==null){
            return false;
        }
        return  true;
    }

    public HttpResponse<String> postWithTokenJsonBody(String url, String jsonBody) throws UnirestException {
        HttpResponse<String> response=Unirest.post(baseUrl+url)
                .header("token",token)
                .header("content-type","application/json")
                .body(jsonBody)
                .asString();
        return  response;

    }

    public RestClient(String baseUrl){

        this.baseUrl=baseUrl;
    }
    public RestClient(int port){
        this.baseUrl="http://localhost:"+port;
    }
    public  RestClient(){
        baseUrl=_baseUrl;
    }


    public String getToken() {
        return token;
    }

    public HttpResponse<String> postWithoutTokenJsonBody(String url, String json) throws UnirestException {
        HttpResponse<String> response=Unirest.post(baseUrl+url)
                .header("content-type","application/json")
                .body(json)
                .asString();

        return  response;

    }
}
