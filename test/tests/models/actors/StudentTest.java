package tests.models.actors;

import models.StudentManager;
import tests.TestWithDataBase;
import models.UniversityManager;
import org.junit.Assert;
import org.junit.Test;
import play.db.DB;

import java.sql.Connection;
import java.util.Calendar;

import static play.test.Helpers.running;

/**
 * Created by kozo on 6/17/16.
 */
public class StudentTest extends TestWithDataBase {
    final String username="py4object",password="1234";

    @Test
    public void testLogin(){
        running(fakeApp,()->{
            Connection c= DB.getConnection();
            UniversityManager.addUniversity("iku");
            int uniId=UniversityManager.getUniversities().get(0).getUniversityId();
            StudentManager.addStudent(username,
                    password,"omar","zahed",uniId,"12345","py4object@gmial.com",
                    new java.util.Date(2000, Calendar.JANUARY,3),"NYc","i love pizza",
                    0,true,new java.util.Date());
            Assert.assertNotNull(StudentManager.getStudentByUserNameAndPassword(username,password));
            Assert.assertNull(StudentManager.getStudentByUserNameAndPassword(username,"notThePassword"));
            Assert.assertNull(StudentManager.getStudentByUserNameAndPassword("NotTheUserName",password));
            Assert.assertNull(StudentManager.getStudentByUserNameAndPassword("NotTheUserName","NotThePassword"));
        });

    }


}
