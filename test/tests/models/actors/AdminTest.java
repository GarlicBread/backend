package tests.models.actors;

import models.AdminManager;
import models.StudentManager;
import models.UniversityManager;
import models.actors.Admin;
import models.actors.Student;
import org.junit.Test;
import tests.TestWithDataBase;

import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.*;
import static play.test.Helpers.running;

/**
 * Created by kozo on 6/18/16.
 */
public class AdminTest extends TestWithDataBase{

    @Test
    public void testSingUp(){
        running(fakeApp,()->{
            boolean result= AdminManager.add("admin1234","pass1234"
                    ,"John","Luke","lukey.jhon@gmail.com",
                    new Date(1990, Calendar.JANUARY,14),"NYC",
                    "i love pizza",true,new Date());
            assertTrue(result);
            result= AdminManager.add("admin1234","pass1234"
                    ,"John","Luke","lukey.jhon@gmail.com",
                    new Date(1990, Calendar.JANUARY,14),"NYC",
                    "I love pizzan",true,new Date());
            assertFalse(result);
        });


    }

    @Test
    public void testlogin(){
        running(fakeApp,()->{
            String userName="adminbobo";
            String password="1234asd";
            AdminManager.add(userName,password
                    ,"John","Luke","lukey.jhon@gmail.com",
                    new Date(1990, Calendar.JANUARY,14),"NYC",
                    " ilove pizza",true,new Date());

            assertNotNull(AdminManager.getAdminByUserNameAndPassword(userName,password));
            assertNull(AdminManager.getAdminByUserNameAndPassword(userName,"NotThePassword"));
            assertNull(AdminManager.getAdminByUserNameAndPassword("NotTheUserName",password));
            assertNull(AdminManager.getAdminByUserNameAndPassword("NotTheUserName","NotThePassword"));
        });


    }

    @Test
    public  void disableUser(){
        String studentUserName="boobo",studentPassword="logasd";
        String adminUserName="asdasdvzx",adminPassword="asdavasd";
        running(fakeApp,()->{
            UniversityManager.addUniversity("iku");
            int uniId=UniversityManager.getUniversities().get(0).getUniversityId();
            StudentManager.addStudent(studentUserName,
                    studentPassword,"omar","zahed",uniId,"12345","py4object@gmial.com",
                    new java.util.Date(2000, Calendar.JANUARY,3),"NYc","i love pizza",
                    0,true,new java.util.Date());
                     AdminManager.add(adminUserName,adminPassword
                    ,"John","Luke","lukey.jhon@gmail.com",
                    new Date(1990, Calendar.JANUARY,14),"NYC",
                    "i love pizza",true,new Date());
            Admin admin= AdminManager.getAdminByUserNameAndPassword(adminUserName,adminPassword);
            assertTrue(admin.changeStudentStatus(studentUserName,false));
            Student student= StudentManager.getStudentByUserName(studentUserName);
            assertFalse(student.isStatus());

        });





    }

}
