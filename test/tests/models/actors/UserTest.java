package tests.models.actors;

import models.AdminManager;
import models.StudentManager;
import models.actors.User;
import models.entities.Message;
import org.junit.Test;
import tests.TestWithDataBase;
import tests.tools.Tools;

import java.util.List;

import static junit.framework.Assert.assertTrue;
import static play.test.Helpers.running;

/**
 * Created by kozo on 6/19/16.
 */
public class UserTest extends TestWithDataBase {
    @Test
    public void testSendMessageTwoAdmins(){
        String recevierUserName="user1";
        String senderUserName="user2";
        String password1="1231asd";
        String senderPassword="adsvacsa";
        String messageSubject="ldaidoasd asd";
        String message="asda asldkasdm asmalsdasdasd";
        running(fakeApp,()->{
            Tools.AddAdmin(recevierUserName,password1);
            Tools.AddAdmin(senderUserName,senderPassword);
            User user= AdminManager.getAdminByUserNameAndPassword(senderUserName,senderPassword);
            Boolean result=user.sentMessageTo(recevierUserName,messageSubject,message);
            assertTrue(result);
            List<Message> sentEmail=user.getSentEmails();
            assertTrue(sentEmail.size()==1);
            assertTrue(sentEmail.get(0).getReciverUserName().equals(recevierUserName));
            assertTrue(sentEmail.get(0).getSenderUserName().equals(senderUserName));
            assertTrue(sentEmail.get(0).getSubject().equals(messageSubject));
            assertTrue(sentEmail.get(0).getBody().equals(message));
            User user1= AdminManager.getAdminByUserNameAndPassword(recevierUserName,password1);
            List<Message> inbox=user1.getInbox();
            assertTrue(inbox.get(0).getSenderUserName().equals(senderUserName));
            assertTrue(inbox.get(0).getReciverUserName().equals(recevierUserName));
            assertTrue(inbox.get(0).getSubject().equals(messageSubject));
            assertTrue(inbox.get(0).getBody().equals(message));

        });
    }

    @Test
    public void testSendMessageStudentToAdmin(){
        String recevierUserName="user1";
        String senderUserName="user2";
        String password1="1231asd";
        String senderPassword="adsvacsa";
        String messageSubject="ldaidoasd asd";
        String message="asda asldkasdm asmalsdasdasd";
        running(fakeApp,()-> {
            Tools.AddAdmin(recevierUserName, password1);
            Tools.AddStudent(senderUserName, senderPassword);
            User user = StudentManager.getStudentByUserNameAndPassword(senderUserName, senderPassword);
            Boolean result = user.sentMessageTo(recevierUserName, messageSubject, message);
            assertTrue(result);
            List<Message> sentEmail = user.getSentEmails();
            assertTrue(sentEmail.size() == 1);
            assertTrue(sentEmail.get(0).getReciverUserName().equals(recevierUserName));
            assertTrue(sentEmail.get(0).getSenderUserName().equals(senderUserName));
            assertTrue(sentEmail.get(0).getSubject().equals(messageSubject));
            assertTrue(sentEmail.get(0).getBody().equals(message));
            User user1 = AdminManager.getAdminByUserNameAndPassword(recevierUserName, password1);
            List<Message> inbox = user1.getInbox();
            assertTrue(inbox.get(0).getSenderUserName().equals(senderUserName));
            assertTrue(inbox.get(0).getReciverUserName().equals(recevierUserName));
            assertTrue(inbox.get(0).getSubject().equals(messageSubject));
            assertTrue(inbox.get(0).getBody().equals(message));
        });

    }
    @Test
    public void sentMessageAdminToStudent(){
        String recevierUserName="user1";
        String senderUserName="user2";
        String password1="1231asd";
        String senderPassword="adsvacsa";
        String messageSubject="ldaidoasd asd";
        String message="asda asldkasdm asmalsdasdasd";
        running(fakeApp,()-> {
            Tools.AddStudent(recevierUserName, password1);
            Tools.AddAdmin(senderUserName, senderPassword);
            User user = AdminManager.getAdminByUserNameAndPassword(senderUserName, senderPassword);
            Boolean result = user.sentMessageTo(recevierUserName, messageSubject, message);
            assertTrue(result);
            List<Message> sentEmail = user.getSentEmails();
            assertTrue(sentEmail.size() == 1);
            assertTrue(sentEmail.get(0).getReciverUserName().equals(recevierUserName));
            assertTrue(sentEmail.get(0).getSenderUserName().equals(senderUserName));
            assertTrue(sentEmail.get(0).getSubject().equals(messageSubject));
            assertTrue(sentEmail.get(0).getBody().equals(message));
            User user1 = StudentManager.getStudentByUserNameAndPassword(recevierUserName, password1);
            List<Message> inbox = user1.getInbox();
            assertTrue(inbox.get(0).getSenderUserName().equals(senderUserName));
            assertTrue(inbox.get(0).getReciverUserName().equals(recevierUserName));
            assertTrue(inbox.get(0).getSubject().equals(messageSubject));
            assertTrue(inbox.get(0).getBody().equals(message));
        });
    }
    @Test
    public void sentMessageTwoStudents(){
        String recevierUserName="user1";
        String senderUserName="user2";
        String password1="1231asd";
        String senderPassword="adsvacsa";
        String messageSubject="ldaidoasd asd";
        String message="asda asldkasdm asmalsdasdasd";
        running(fakeApp,()-> {
            Tools.AddStudent(recevierUserName, password1);
            Tools.AddStudent(senderUserName, senderPassword);
            User user = StudentManager.getStudentByUserNameAndPassword(senderUserName, senderPassword);
            Boolean result = user.sentMessageTo(recevierUserName, messageSubject, message);
            assertTrue(result);
            List<Message> sentEmail = user.getSentEmails();
            assertTrue(sentEmail.size() == 1);
            assertTrue(sentEmail.get(0).getReciverUserName().equals(recevierUserName));
            assertTrue(sentEmail.get(0).getSenderUserName().equals(senderUserName));
            assertTrue(sentEmail.get(0).getSubject().equals(messageSubject));
            assertTrue(sentEmail.get(0).getBody().equals(message));
            User user1 = StudentManager.getStudentByUserNameAndPassword(recevierUserName, password1);
            List<Message> inbox = user1.getInbox();
            assertTrue(inbox.get(0).getSenderUserName().equals(senderUserName));
            assertTrue(inbox.get(0).getReciverUserName().equals(recevierUserName));
            assertTrue(inbox.get(0).getSubject().equals(messageSubject));
            assertTrue(inbox.get(0).getBody().equals(message));
        });
    }
}
