package tests.models.entites;

import models.*;
import models.actors.Student;
import models.entities.Note;
import org.junit.Assert;
import org.junit.Test;
import tests.TestWithDataBase;
import tests.tools.Tools;

import java.util.Date;

import static play.test.Helpers.running;

/**
 * Created by kozo on 6/26/16.
 */
public class NoteTest extends TestWithDataBase {

    @Test
    public void testPostANote(){
        String username="usewr1234";
        String title="\"Funny note\"";
        String text="I like Funny stuff";
        running(fakeApp,()->{
            Tools.AddStudent(username,"123425");
            Student student= StudentManager.getStudentByUserName(username);
            int uniId=UniversityManager.getUniversities().get(0).getUniversityId();
            DepartmentManager.addDepartment("Computer Engineering");
            int depId=DepartmentManager.getDepartments().get(0).getDepartmentId();
            CourseManager.addCourse("Programming");
            int courseId=CourseManager.getCourses().get(0).getCourseId();
            Uni_DepManager.addUniDep(uniId,depId);
            int UniDep=Uni_DepManager.getUniDeps().get(0).getUniDepId();
            Course_UniDepManager.addCourseUniDep(UniDep,courseId);
            int courseUniDepId=Course_UniDepManager.getCourseUniDeps().get(0).getCourseUniDepId();
            NoteManager.addNote(
                    username,courseUniDepId,
                    title,
                    text,
                    true,
                    new Date()
            );
            Note note =NoteManager.getNotes().get(0);
            Assert.assertNotNull(note);
            Assert.assertTrue(username.equals(note.getUserName()));
            Assert.assertTrue(note.getUpVoteCounter()==0);
            Assert.assertTrue(note.getDownVoteCounter()==0);
            Assert.assertTrue(note.getTitle().equals(title));
            Assert.assertTrue(note.getText().equals(text));

            String user2="ssuadc";
            String user3="SAdas";

            Tools.AddStudent(user2,"asdasd");
            Tools.AddStudent(user3,"asdaf");

            Student suser3=StudentManager.getStudentByUserName(user3);
            Student suser2=StudentManager.getStudentByUserName(user3);

            note.upVoteBy(user2);
            note.downVoteBy(user3);

            Assert.assertTrue(note.getUpVoteCounter()==1);
            Assert.assertTrue(note.getDownVoteCounter()==1);
            Assert.assertFalse(note.getVoters().get(user3));
            Assert.assertTrue(note.getVoters().get(user2));
            note.upVoteBy(user2);
            note.downVoteBy(user3);
            Assert.assertTrue(note.getUpVoteCounter()==1);
            Assert.assertTrue(note.getDownVoteCounter()==1);

            //from Database
            note=NoteManager.getNote(note.getNodeId());
            Assert.assertTrue(note.getUpVoteCounter()==1);
            Assert.assertTrue(note.getDownVoteCounter()==1);
            Assert.assertFalse(note.getVoters().get(user3));
            Assert.assertTrue(note.getVoters().get(user2));





        });

    }

}
