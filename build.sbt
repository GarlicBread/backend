name := """worldofuni"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  javaJdbc,
  cache,
  javaWs
)
libraryDependencies += "org.postgresql" % "postgresql" % "9.4.1208"

libraryDependencies += "org.apache.commons" % "commons-lang3" % "3.0"
libraryDependencies += "com.mashape.unirest" % "unirest-java" % "1.4.5"

